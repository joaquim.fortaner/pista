from led import Led
from game import Game
from flash import Flash
from connect import Connect


class Control:
    def __init__(self):
        self.array_led = []
        self.connect = Connect()
        self.__game = Game(1, self.connect, self.array_led)
        self.__flash = Flash(1, self.connect, self.array_led)

        for i in range(10):
            led = Led(i)
            self.array_led.append(led)

    def action(self, number):
        if number == 0:
            self.__game.job_ended = True
            self.__flash.job_ended = True
        elif number == 1:
            self.__game.job_ended = True
            self.__flash.job_ended = True
            red = 0
            green = 0
            blue = 255
            self.change_all(red, green, blue)
        elif number == 2:
            self.__game.job_ended = True
            self.__flash.job_ended = True
            red = 0
            green = 0
            blue = 0
            self.change_all(red, green, blue)
        elif number == 3:
            self.__game.job_ended = True
            self.__flash = Flash(0.05, self.connect, self.array_led)
            self.__flash.start()
        elif number == 4:
            self.__flash.job_ended = True
            self.__game = Game(0.5, self.connect, self.array_led)
            self.__game.read()
            self.__game.start()

    def change_all(self, red, green, blue):
        for led in self.array_led:
            led.change_colour(red, green, blue)
            self.connect.send(led.codification())
