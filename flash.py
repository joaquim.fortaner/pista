from threading import Thread
import time


class Flash(Thread):
    def __init__(self, delay, connect, array_led):
        self.delay = delay
        self.connect = connect
        self.array_led = array_led
        self.job_ended = False
        Thread.__init__(self)

    def run(self):
        while True:
            if self.job_ended:
                return

            for led in self.array_led:
                led.change_colour(255, 255, 255)
                self.connect.send(led.codification())

            time.sleep(self.delay)

            if self.job_ended:
                return

            for led in self.array_led:
                led.change_colour(0, 0, 0)
                self.connect.send(led.codification())

            time.sleep(self.delay)
