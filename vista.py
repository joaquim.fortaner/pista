from tkinter import *
from control import Control


def btn_click(number):
    if number == 0:
        control.action(number)
        window.destroy()
    elif number == 1:
        input_text.set("On")
        control.action(number)
    elif number == 2:
        input_text.set("Off")
        control.action(number)
    elif number == 3:
        input_text.set("Flash")
        control.action(number)
    elif number == 4:
        input_text.set("Game")
        control.action(number)


control = Control()

window = Tk()
window.overrideredirect(True)
window.geometry("{0}x{1}+0+0".format(window.winfo_screenwidth(), window.winfo_screenheight()))
window.configure(background="aqua")

button_0 = Button(window, bd=6, bg="linen",   text="Exit",  command=lambda: btn_click(0))
button_1 = Button(window, bd=6, bg="blue",    text="On",    command=lambda: btn_click(1))
button_2 = Button(window, bd=6, bg="black",   text="Off",   command=lambda: btn_click(2))
button_3 = Button(window, bd=6, bg="white",   text="Flash", command=lambda: btn_click(3))
button_4 = Button(window, bd=6, bg="green",   text="Game",  command=lambda: btn_click(4))

button_0.place(anchor=N, relwidth=0.96,                relx=0.5,  rely=0.9)
button_1.place(anchor=N, relwidth=0.24, relheight=0.2, relx=0.14, rely=0.5)
button_2.place(anchor=N, relwidth=0.24, relheight=0.2, relx=0.38, rely=0.5)
button_3.place(anchor=N, relwidth=0.24, relheight=0.2, relx=0.62, rely=0.5)
button_4.place(anchor=N, relwidth=0.24, relheight=0.2, relx=0.86, rely=0.5)

widget = Label(window, bg="aqua", font=("arial", 20), text="CONTROL")
widget.place(anchor=N, relx=0.5, rely=0.1)

input_text = StringVar()
text = Entry(window, bg="snow", font=("arial", 20), justify="center", textvariable=input_text)
text.place(anchor=N, relwidth=0.96, relx=0.5, rely=0.3)

window.mainloop()
