from threading import Thread
import time


class Game(Thread):
    def __init__(self, delay, connect, array_led):
        self.delay = delay
        self.connect = connect
        self.array_led = array_led
        self.colors_led = []
        self.job_ended = False
        Thread.__init__(self)

    def read(self):
        f = open("colors.txt", "r")
        colors = f.read()
        f.close()

        colors = colors.split()
        colors_i = []
        cont = 0

        for color in colors:
            colors_i.append(int(color))
            cont += 1
            if cont == 4:
                self.colors_led.append(colors_i)
                colors_i = []
                cont = 0

    def run(self):
        while True:
            for games in self.colors_led:

                if games[0] == 1:
                    if self.job_ended:
                        return
                    for led in self.array_led:
                        led.change_colour(games[1], games[2], games[3])
                        self.connect.send(led.codification())
                    time.sleep(self.delay)

                elif games[0] == 2:
                    for led in self.array_led:
                        if self.job_ended:
                            return
                        led.change_colour(games[1], games[2], games[3])
                        self.connect.send(led.codification())
                        time.sleep(self.delay)
