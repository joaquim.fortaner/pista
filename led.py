class Led:
    def __init__(self, pos):
        self.__pos = pos
        self.__red = 0
        self.__green = 0
        self.__blue = 0

    def set_red(self, red):
        if -1 < red < 256:
            self.__red = red

    def set_green(self, green):
        if -1 < green < 256:
            self.__green = green

    def set_blue(self, blue):
        if -1 < blue < 256:
            self.__blue = blue

    def change_colour(self, red, green, blue):
        self.set_red(red)
        self.set_green(green)
        self.set_blue(blue)

    def codification(self):
        #b_pos = bytes([self.__pos])
        #b_red = bytes([self.__red])
        #b_green = bytes([self.__green])
        #b_blue = bytes([self.__blue])

        #b = b_pos + b_red + b_green + b_blue
        #return b

        return f'led: {self.__pos}, R: {self.__red}, G: {self.__green}, B: {self.__blue}'
